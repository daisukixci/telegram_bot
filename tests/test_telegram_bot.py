"""
Test for BotHandler
"""
import unittest
from unittest.mock import Mock

import os
from telegram_bot import BotHandler
from telegram_bot import __version__


def test_version():
    assert __version__ == "0.1.0"


class TestBotHandler(unittest.TestCase):
    """
    Unit tests for BotHandler
    """

    def setUp(self):
        self.help_menu = """Let me help you:
        /start
        /hi"""
        mock_token = Mock()
        self.bot_handler = BotHandler(mock_token)

    def test_get_answer(self):
        """
        Test generate answer
        """

        expected_answer_hi = {"action": "message", "message": "Hello You"}
        expected_answer_help = {"action": "message", "message": self.help_menu}

        self.assertEqual(self.bot_handler._get_answer("/hi"), expected_answer_hi)
        self.assertEqual(self.bot_handler._get_answer("/?"), expected_answer_help)

    def test_get_docker_secret(self):
        """
        Test get docker secret
        """
        os.environ["FOO"] = "FOO"
        self.assertEqual(self.bot_handler._get_docker_secret("foo"), "FOO")
        self.assertEqual(self.bot_handler._get_docker_secret("none"), "")
        self.assertEqual(
            self.bot_handler._get_docker_secret("none", default="default"), "default"
        )

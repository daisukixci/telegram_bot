#!/usr/bin/env python3
"""
Telegram bot that can:
    - get conversation updates
    - send message
"""

from __future__ import annotations

import os
import time
import threading

import yaml
import requests
from requests.compat import urljoin


class BotHandler(threading.Thread):
    """
    BotHandler is a class which implements all back-end of the bot.
    It has three main functions:
        'get_updates' — checks for new messages
        'send_message' – posts new message to user
        'get_answer' — computes the most relevant on a user's question
    """

    def __init__(self, token: str = "", name: str = "Bot", config: str = ""):
        threading.Thread.__init__(self)
        self.name = name
        self.config = self._load_conf(config)
        self.token = self._get_docker_secret(
            self.config.get("telegram_api_token", "telegram_api_token")
        )
        if token:
            self.token = token
        # TODO: Check connectivity
        self._api_url = f"https://api.telegram.org/bot{self.token}/"
        self._help_menu = """Let me help you:
        /start
        /hi"""

    def _load_conf(self, config_path: str):
        """
        Load configuration from filepath designed file

        :param config_path: Configuration file path
        """
        if not config_path:
            return {}

        with open(config_path, "r", encoding="UTF-8") as stream:
            try:
                config = yaml.safe_load(stream)
            except yaml.YAMLError as error:
                print("Error loading configuration file %s", error)
                return {}
            # TODO: add configuration checks

        return config

    def _get_docker_secret(
        self,
        name: str,
        default: str = "",
        getenv: bool = True,
        secrets_dir: str = "/var/run/secrets",
    ) -> str:
        """This function fetches a docker secret
        :param name: the name of the docker secret
        :param default: the default value if no secret found
        :param getenv: if environment variable should be fetched as fallback
        :param secrets_dir: the directory where the secrets are stored
        :returns: docker secret or environment variable depending on params
        """
        name_secret = name.lower()
        name_env = name.upper()

        # initialize value
        value = ""

        # try to read from secret file
        try:
            with open(
                os.path.join(secrets_dir, name_secret), "r", encoding="UTF-8"
            ) as secret_file:
                value = secret_file.read().strip()
        except IOError:
            # try to read from env if enabled
            if getenv:
                value = os.environ.get(name_env)

        # set default value if no value found
        if not value:
            value = default

        return value

    def _get_updates(self, offset: int = 0, timeout: int = 30) -> list:
        """
        Gets update from conversation

        :param offset:
        :param timeout: Timeout value for getting conversation
        :return: List of response
        :raises ValueError: If json parsing of the response fails
        """
        params = {"timeout": timeout, "offset": offset}
        raw_resp = requests.get(
            urljoin(self._api_url, "getUpdates"), params, timeout=30
        )
        try:
            resp = raw_resp.json()
        except ValueError as error:
            raise ValueError(f"Failed to parse response {raw_resp.content}") from error

        if "result" not in resp:
            return []
        return resp["result"]

    def _send_message(self, chat_id: str, text: str) -> requests.Response:
        """
        Send text into chat with chat_id

        :param chat_id: ID of the Telegram chat
        :param text: Message to send on chan_id
        """
        params = {"chat_id": chat_id, "text": text}
        return requests.post(urljoin(self._api_url, "sendMessage"), params, timeout=5)

    def _get_answer(self, chat_id: str, question: str) -> dict[str, str]:
        """
        Generate the answer according to the question

        :param question: Command/question in the chat
        """
        answer = {}
        if question.lower() == "/start":
            answer = {
                "action": "message",
                "message": f"Hi, I am {self.name}. You are in chat {chat_id}. How can I help you?\nUse /? to get more information",
            }

        if "hi" in question.lower():
            answer = {
                "action": "message",
                "message": f"Hello You! You are in chat {chat_id}.",
            }

        if question == "/?":
            answer = {"action": "message", "message": self._help_menu}

        return answer

    def run(self):
        offset = 0
        chat_id = ""
        while True:
            # Wait a message and answer
            for update in self._get_updates(offset=offset):
                if "message" in update:
                    chat_id = update["message"]["chat"]["id"]
                    if "text" in update["message"]:
                        answer = self._get_answer(chat_id, update["message"]["text"])
                        try:
                            self._send_message(chat_id, answer.get("message", "Error"))
                        except KeyError:
                            print("Message key is missing")

                offset = max(offset, update["update_id"] + 1)
            time.sleep(1)

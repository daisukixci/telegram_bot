all: test run

run:
	python main.py

test:
	poetry run pytest -v tests
	poetry run pytest --cov=. tests

clean:
	find . -name "*.pyc" -delete
	find . -name "__pycache__" -delete

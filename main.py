#!/usr/bin/env python3
"""
A simple bot for Telegram conversation that enables to:
    - Say hi
"""

from telegram_bot import BotHandler


def main():
    """
    Handler
    """
    bot = BotHandler()
    bot.start()


if __name__ == "__main__":
    main()
